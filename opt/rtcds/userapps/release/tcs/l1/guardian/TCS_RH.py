from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager
from guardian.ligopath import userapps_path
import cdsutils as cdu
import ifoconfig
import tcsconfig

# initial request on initialization
request = 'RH_DEFAULT'
# nominal operating state
nominal = 'FINAL_HIGHPOWER'
#nominal = 'RH_DEFAULT'
####################

def MC_is_locked():
    return ezca['IMC-IMC_TRIGGER_INMON'] >= 35

#newnode
nodes = NodeManager([
    'TCS_ITMX_RH_PWR',
    'TCS_ITMY_RH_PWR',
])
#    'TCS_ETMX_RH_PWR',
#    'TCS_ETMY_RH_PWR',


####################
# STATES
####################

class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        log('Initializing ring heater guardian:')
        nodes.set_managed()
        return True

    def run(self):
        return True


class STOP(GuardState):
    """
    Stop ring heater transients and return to default state

    """
    index = 2
    goto = True
    request = True
    def main(self):
        return True

    def run(self):
        return True


class RH_DEFAULT(GuardState):
    """
    Set ring heaters to lock aquisition settings

    """
    index = 1
    #goto = True  # Use stop state to jump out of timers
    request = True
    def main(self):
        nodes['TCS_ITMX_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING' # Options 'UNFILTERED_RH_INPUT' 
        nodes['TCS_ITMY_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING' #    'FILTER_RH_INPUT_NOOVERSHOOT'
        #nodes['TCS_ETMX_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING' #    'FILTER_RH_INPUT_FASTEST'
        #nodes['TCS_ETMY_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING' #    'FILTER_RH_INPUT_SURFACE'

        ezca['TCS-ITMX_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.itmx_rh_power
        ezca['TCS-ITMY_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.itmy_rh_power
        ezca['TCS-ETMX_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.etmx_rh_power
        ezca['TCS-ETMY_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.etmy_rh_power
        return True

    def run(self):
        return True


class PREPARE_HIGHPOWER(GuardState):
    """
    Ring heater setting for the power transient and synchronisation
    """
    index = 4

    def main(self):
        # Make sure the ITM ring heater inverse filters are the one required to match the self heating traneint
        nodes['TCS_ITMX_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING'
        nodes['TCS_ITMY_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING'
        # Make sure the ETM ring heater inverse filters do the same thing as the ITMs
        #nodes['TCS_ETMX_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING'
        #nodes['TCS_ETMY_RH_PWR'] = 'FILTER_RH_INPUT_SELFHEATING'

        ezca['TCS-ITMX_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.itmx_rh_highpower  
        ezca['TCS-ITMY_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.itmy_rh_highpower
        ezca['TCS-ETMX_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.etmx_rh_highpower
        ezca['TCS-ETMY_RH_INVERSE_FILTER_OFFSET'] = tcsconfig.etmy_rh_highpower

        # Set timer for 15 min to synchronise power up with ring heater transient
        #self.timer['pi_tcs1'] = 15*60
        self.timer['pi_tcs1'] = 10
        return True

    def run(self):

        if self.timer['pi_tcs1']:
            #Ready to power up (to synchronise power up with ring heater lens transient)
            return True


class FINAL_HIGHPOWER(GuardState):
    """
    Final state for ring heaters
    """
    index = 14
    request = True
    def main(self):

        return True

    def run(self):
  
        return True


class READY_HIGHPOWER(GuardState): 
    """
    Ready to power up (to synchronise power up with ring heater lens transient)
    """
    index = 8
    def main(self):
        self.timer['pi_tcs2'] = 10

    def run(self):
        if not MC_is_locked():
            log('MC lost lock!')
            return 'RH_DEFAULT'
        if self.timer['pi_tcs2']:
            return True


class COOL_HIGHPOWER(GuardState): # Not using this state
    """
    Cool down after a power45 Watt lock 
    """
    index = 19
    def main(self):
        IXRHpower = 0     -  0   * (ifoconfig.power45 - 25) / 17 # Use to transfer ring heater power to ITMX # CB 20180214
        IYRHpower = 0     -  0   * (ifoconfig.power45 - 25) / 17 # Use to transfer ring heater power to ITMY
        EXRHpower = 0.59  -  0.1 * (ifoconfig.power45 - 25) / 17 # 1.2W ring heater worked for a power step from 25W to 42W
        EYRHpower = 0.5   -  0.1 * (ifoconfig.power45 - 25) / 17 # 1.2W ring heater worked for a power step from 25W to 42W
        # ITMs
        ezca['TCS-ITMX_RH_SETUPPERPOWER'] = IXRHpower
        ezca['TCS-ITMX_RH_SETLOWERPOWER'] = IXRHpower 
        ezca['TCS-ITMY_RH_SETUPPERPOWER'] = IYRHpower
        ezca['TCS-ITMY_RH_SETLOWERPOWER'] = IYRHpower
        # ETMs
        ezca['TCS-ETMX_RH_SETUPPERPOWER'] =  EXRHpower    #CB reduce by 40/42 20180209, 1.1 Value for 42W.  Original values set in ISC_LOCK down 0.581 #0.50 #JCB 2017/02/14 Calculation ~3W at 50W
        ezca['TCS-ETMX_RH_SETLOWERPOWER'] =  EXRHpower    #1.1 Value for 42W.  Original values set in ISC_LOCK down 0.603 #0.50 #JCB 
        ezca['TCS-ETMY_RH_SETUPPERPOWER'] =  EYRHpower    #1.2 Value for 42W.  Original values set in ISC_LOCK down 0.5
        ezca['TCS-ETMY_RH_SETLOWERPOWER'] =  EYRHpower    #1.2 Value for 42W.  Original values set in ISC_LOCK down 0.5
        self.timer['pi_tcs3'] = 7200 

    def run(self):
        #if (ezca['GRD-ISC_LOCK_STATE_N'] >= 1191 and ezca['GRD-ISC_LOCK_REQUEST_N'] >= 1194 ):
        #    log('Power back up again from cool down state')
        #    return 'RH_DEFAULT'
        if self.timer['pi_tcs3']:
            return 'RH_DEFAULT'


class PREPARE_25W(GuardState):
    """
    initial settings for ring heaters for beginning of 50W lock
    """
    index = 27
    def main(self):
# The ratio of the ITm to ETM heating is scaled currently to reflect the measured absorption coefficients reported https://dcc.ligo.org/cgi-bin/private/DocDB/ShowDocument?docid=T1400685&version=0
# The absorption estimates are ITMX 0.2ppm, ITMY 0.14ppm, ETMX 0.5ppm and ETMY 1.6ppm. To maintain ITMX to ITMY this ratio is fixed to 0.14/0.2.  To maintain arm g-factor the average (ITMX+ITMY) / (ETMX+ETMY) =0.25 is used to fix the ITM value average then a correction is applide to get the same ITM common lens , Not implimented in tis state.
        IXRHpower = 1.3 * 0 * (25) / 17 # Use to transfer ring heater power to ITMX
        IYRHpower = 1.4 * 0 * (25) / 17 # Use to transfer ring heater power to ITMY
        EXRHpower = 1.3 * 1 * (25) / 17 # Estimate power from 50W transitions CB 2018025
        EYRHpower = 1.4 * 1 * (25) / 17 # 
        # ITMs
        ezca['TCS-ITMX_RH_SETUPPERPOWER'] = IXRHpower    
        ezca['TCS-ITMX_RH_SETLOWERPOWER'] = IXRHpower
        ezca['TCS-ITMY_RH_SETUPPERPOWER'] = IYRHpower
        ezca['TCS-ITMY_RH_SETLOWERPOWER'] = IYRHpower
        # ETMs
        ezca['TCS-ETMX_RH_SETUPPERPOWER'] = EXRHpower    
        ezca['TCS-ETMX_RH_SETLOWERPOWER'] = EXRHpower    
        ezca['TCS-ETMY_RH_SETUPPERPOWER'] = EYRHpower    
        ezca['TCS-ETMY_RH_SETLOWERPOWER'] = EYRHpower    
        # Set timer for 15 min
        self.timer['pi_tcs4'] = 800 

#        # ITMs
#        ezca['TCS-ITMX_RH_SETUPPERPOWER'] = 0.0     #CB back to nominal 20180209 #15% extra on ITMY (0.52) CB 8Feb2018 % TH 0.45 on ITMs, 1.05 and 1.15 on ETMs 2/8/2018
#        ezca['TCS-ITMX_RH_SETLOWERPOWER'] = 0.0
#        ezca['TCS-ITMY_RH_SETUPPERPOWER'] = 0.0
#        ezca['TCS-ITMY_RH_SETLOWERPOWER'] = 0.0     # 0.52
        # ETMs
#        ezca['TCS-ETMX_RH_SETUPPERPOWER'] = 1.53     # 1.05 
#        ezca['TCS-ETMX_RH_SETLOWERPOWER'] = 1.53     #    
#        ezca['TCS-ETMY_RH_SETUPPERPOWER'] = 1.63     # 1.15   
#        ezca['TCS-ETMY_RH_SETLOWERPOWER'] = 1.63     #      
        # Set timer for 15 min
#        self.timer['pi_tcs4'] = 800 

    def run(self):
        if not MC_is_locked():
            log('MC lost lock!')
            return 'RH_DEFAULT'
        if self.timer['pi_tcs4']:
            return True


class READY_25W(GuardState):
    """
    Ready to power up to 50W lock
    """
    index = 21
    def main(self):
         self.timer['pi_tcs4'] = 6400 #1.75 hours CB passed timefrom old previous state 5800 TH 20180210 #7200

    def run(self):
        if not MC_is_locked():
            log('MC lost lock!')
            return 'RH_DEFAULT'
        if self.timer['pi_tcs4']:
            return True


class FINAL_25W(GuardState):
    """
    final settings for ring heaters for post-transient steady state 50W lock
    """
    index = 31
    request = True
    def main(self):
        IXRHpower = 1.1 * 0 * (25) / 17 # Use to transfer ring heater power to ITMX
        IYRHpower = 1.1 * 0 * (25) / 17 # Use to transfer ring heater power to ITMY
        EXRHpower = 1.2 * 1 * (25) / 17 # Estimate power from 50W transitions CB 2018025
        EYRHpower = 1.2 * 1 * (25) / 17 # 
        # ITMs
        ezca['TCS-ITMX_RH_SETUPPERPOWER'] = IXRHpower    
        ezca['TCS-ITMX_RH_SETLOWERPOWER'] = IXRHpower
        ezca['TCS-ITMY_RH_SETUPPERPOWER'] = IYRHpower
        ezca['TCS-ITMY_RH_SETLOWERPOWER'] = IYRHpower
        # ETMs
        ezca['TCS-ETMX_RH_SETUPPERPOWER'] = EXRHpower    
        ezca['TCS-ETMX_RH_SETLOWERPOWER'] = EXRHpower    
        ezca['TCS-ETMY_RH_SETUPPERPOWER'] = EYRHpower    
        ezca['TCS-ETMY_RH_SETLOWERPOWER'] = EYRHpower    

    def run(self):
        if not MC_is_locked():
            log('MC lost lock!')
            return 'RH_DEFAULT'
        return True



edges = [
    ('INIT','RH_DEFAULT'),
    ('STOP','RH_DEFAULT'),
#    ('RH_DEFAULT','PREPARE_25W'),
#    ('PREPARE_25W','READY_25W'),
#    ('READY_25W','FINAL_25W'),
    ('RH_DEFAULT','PREPARE_HIGHPOWER'),
#    ('PREPARE_HIGHPOWER','READY_HIGHPOWER'),
#    ('READY_HIGHPOWER','FINAL_HIGHPOWER'),
    ('PREPARE_HIGHPOWER','FINAL_HIGHPOWER'),
    ('FINAL_HIGHPOWER','RH_DEFAULT'),
#    ('COOL_HIGHPOWER','RH_DEFAULT')
    ]

