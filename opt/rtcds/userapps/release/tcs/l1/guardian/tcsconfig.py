#######
#TCS PARAMETERS FILE
#######

starting_priorval = {   'ITMX' : 0.2,
                        'ITMY' : 0.3,
                        'ETMX' : 0.53,
                        'ETMY' : 0.25,
                        }

##########################################
#Below are all legacy
##########################################
power_meter_output_low = {'TCS_ITMX':1.48,'TCS_ITMY':1.48}
power_meter_output_high = {'TCS_ITMX':1.52,'TCS_ITMY':1.52}

#RING HEATER SETTINGS
itmx_rh_power = 0.33 # 0.5 O4a settings
itmy_rh_power = 0.26 # 0.4 O4a settings

#56W:
etmx_rh_power = 0.30 # Changed on Nov 21 2024 KR # 0.20 # 0.44 
etmy_rh_power = 0.05 # Changed on Nov 21 2024 KR # 0.11 
#64W:
#etmx_rh_power = 0.445 # 0.45 # 0.93 # 1.025 # 0.95  #Estimate required for 64W #1.1 Estimate required for 72W 
#etmy_rh_power = 0.105 # 0.18 # 0.23 # 0.325 # 0.30  #Estimate required for 64W #1.2 Estimate required for 72W
#71W:
#etmx_rh_power = 0.45 # 0.50 AE240812 TCS test of common less ETM
#etmy_rh_power = 0.10 # 0.15


#Full power ring heater settings

#56W:
itmx_rh_highpower = 0.41 # 0.61 
itmy_rh_highpower = 0.30 # 0.50 
#64W:
#itmx_rh_highpower = 0.64 # 0.60 raise RF #nominal, # 0.4 #
#itmy_rh_highpower = 0.52 # 0.47 raise RF #nominal # 0.5 # 
#71W:
#itmx_rh_highpower = 0.54 #0.64 #0.74 #lower value for ~2.4 CO2, higher for ~2.0 CO2
#itmy_rh_highpower = 0.42 #0.52 #0.62
#
etmx_rh_highpower = etmx_rh_power # 1.8 other side setting # 0.95 for 10kHz PI #
etmy_rh_highpower = etmy_rh_power # 1.15 other side setting # 0.8 for 10kHz PI #

######
# KF PARAMETERS
######

# Ring Heater params
D1_rh = 0.3612
D2_rh = 1.9300
N1_rh = 0.3612
N2_rh = 2.5660

# Self Heating params
D1_self = 0.3521
D2_self = 1.912
N1_self = 0.3523
N2_self = 1.4245

# CO2 Laser params
D1_co2 = 1.1961
D2_co2 = 2.1905
N1_co2 = 1.1865
N2_co2 = 1.4303
