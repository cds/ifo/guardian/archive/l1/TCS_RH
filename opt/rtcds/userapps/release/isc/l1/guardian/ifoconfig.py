##################################################
# aLIGO IFO PARAMETERS FILE
##################################################


##################################################
# DARM ACTUATION settings

# Which ESD do you want to use for LOW NOISE
lownoise_esd = 'ETMX'

# Which ESD(s) do you want to use for lock acquistion, if you want to use both ESDs include both.
locking_esds = ["ETMX","ETMY"]

# Set this to true if you want to switch ESD to low noise while driving it
lnlv_switch_on_current_esd = True

esd_lownoise_l2lgain = {'ETMX':541,'ETMY':530}	#JCB20240514 ETMY changed from 562 to 530

# ETMY BIAS SIGN FLIPPING
esdx_sign_flip = -1
esdy_sign_flip = +1
#esd_sign_flip = {'ETMX':-1,'ETMY':+1}


# Darm actuator is the ETM for which we feed back to the higher stages (M0,L1,L2)
darm_actuator = 'ETMX'	#Nominally ETMY, Options ETMX, ETMY, BOTH # BOTH currently does not work due to fight with common mode.
darm_actuators = ["ETMX"]

l1_lock_l_gains = {"ETMX":0.9,"ETMY":0.9}
l1_lock_l_gain = 0.9

# To split the L1 and L2 darm drive between ETMX and ETMY
split_darm_stage = 'L1'

##################################################

# Runs the TMS dither and ETM camera spot alignment for green (ALS)
align_grn = True

# TCS - HWS? CO2?
run_tcs = True
run_co2 = False

# SRCL actuator
#srcl_actuator = "SR2"	#nominal
srcl_actuator = "SRM"

# Run OMC Autolocker?
omc_autolock = True

# Center Spots Using Cameras
cam_spot_centering = True

# POP X WFS for PRC2
popx = True

# ADS for IM4 dither offset reduction
dhardIm4Ads = False

# DC readout fringe side (nominally -1)
fringe_side = -1

#FIXME: This might not be needed anymore
darm_offset = -7.0


# ISS gain at 40W
iss_gain_40W = 24

# QUAD A2L gains 

# March 24, 2023 ~2:30 utc f ~20 Hz
#l2_a2l_gains = {'ETMX':{'P':0.65,'Y':0.88},
#                'ETMY':{'P':-0.11,'Y':1.15},
#                'ITMX':{'P':0.0,'Y':0.55},
#                'ITMY':{'P':0.13,'Y':-1.64}}

# March 28, 2023 ~2:30 utc f ~12.5 Hz
#  {'ETMX':{'P':1.5,'Y':0.95},
#   'ETMY':{'P':-2.25,'Y':0.95},
#   'ITMX':{'P':-0.13,'Y':0.50},
#   'ITMY':{'P':0.00,'Y':-1.80}}

# May 17 retuned ITM a2l after BS spot move after camera shift
#l2_a2l_gains = {'ETMX':{'P':1.5,'Y':0.95},
#                'ETMY':{'P':-2.25,'Y':0.95},
#                'ITMX':{'P':-0.13,'Y':-0.20},
#                'ITMY':{'P':0.00,'Y':-1.10}}

# August 16 retuned ITM a2l
#l2_a2l_gains = {'ETMX':{'P':1.5,'Y':0.68},
#                'ETMY':{'P':-2.20,'Y':1.15},
#                'ITMX':{'P':-0.25,'Y':0.10},
#                'ITMY':{'P':-0.06,'Y':-1.10}}

# March ER16 tunning
l2_a2l_gains = {'ETMX':{'P':2.1,'Y':-0.73},  # was 0.4 yaw for -2 offset #yaw was -1 for -6 offset #VB20240904 a2l P 1.4->1.9 test
                'ETMY':{'P':0.72,'Y':1.0},
                'ITMX':{'P':-0.29,'Y':-0.40},
                'ITMY':{'P':-0.04,'Y':-0.94}}

# List of violin modes to check before switching to OMC readout
# In context of SUS-{quad}_L2_DAMP_MODE{number}
violin_list = {'ITMX':[3,4,5,11,12,13,14],'ITMY':[3,4,5,6,11,12,13,14],'ETMX':[3,4,5,6,11,12,13,14],'ETMY':[3,4,5,6,11,12,13,14]}

##################################################
# INPUT

##################################################

# threshold
imc_refl_no_light_threshold = 15


##################################################
# DRMI settings
drmi_locked_threshold_pop18i = 500 # was 800, lowered for microseism AE20240326 #was 4000 before the ND was added 20181116

prmisb_locked_threshold = 600
prmicar_locked_threshold = 5

#
michdark_locked_threshold = 0.02  
michbright_locked_threshold = 0.07 
#
prx_locked_threshold = 25
sry_locked_threshold = 20
srx45_locked_threshold = 5
#
srmi_locked_threshold_ascsum = 200  #100 CB 180531 50 changed to allow larger mich offsets CB 180529
#
refl_45Q_mich = 1
refl_9I_prcl = 1
refl_9I_srcl = 0.42
refl_45I_srcl = 1
#
refl_135Q_mich = 4.5
refl_27I_prcl = 0.6
refl_27I_srcl = 0.18
refl_135I_srcl = 5.2	

##################################################
# FULL LOCK
# set the 'final' power 
power45 = 44


# CARM OFFSETs
#carm_offset_diff_servo   = -7.5	#No longer use AJM 20161129
#carm_offset_qpds_engage  = -12.0	#No longer use AJM 20161129
carm_offset_asq          = -18
carm_offset_dhard        = -90
carm_offset_refl9        = -150
carm_offset_nudge2zero   = -200

engage_slow_wfs_wait     = 3


diff_beatnote_threshold = -30



# Calibration lines
#Modified JCB 2016/11/15, new lower frequency lines; Lines Values put back by shivaraj on 10/28/2016 4:00 CST
#JCB 2019/01/15, new line scheme L1 = line 1, L2 = line 2, L3 = line 3
#JCB 2022/05/16, reducing lines for lower low frequency noise
#UIM line
calibration_line1_freq = 15.1 #22.7#17.8 #33.7
calibration_line1_clkgain = 3.4 #7.5 #15#30 
#PUM line
calibration_line2_freq = 15.7 #22.7#17.8 #33.7
calibration_line2_clkgain = 0.0064 #0.013 #0.026#0.08 #
#ESD only line
calibration_line3_freq = 16.9 #23.9#20.5 #35.3 
calibration_line3_clkgain = 0.0087 #0.017 #0.05#0.150


etmy_lkin_p_freq = 35.7
etmy_lkin_p_clkgain = 0.0117
etmy_lkin_y_freq = 538.7
etmy_lkin_y_clkgain = 0.9703

# Pcal lines
pcalx_line1_freq = 1503.1
pcalx_line1_amp = 10000.0#0#31170.0
pcalx_line5_freq = 33.93
pcalx_line5_amp = 10
pcalx_line6_freq = 54.17
pcalx_line6_amp = 15
pcalx_line7_freq = 78.23
pcalx_line7_amp = 25
pcalx_line8_freq = 101.63
pcalx_line8_amp = 40
pcalx_line9_freq = 283.41
pcalx_line9_amp = 200#1800
pcalx_line10_freq = 434.4
pcalx_line10_amp = 7200
pcaly_line1_freq = 16.3 #23.3#18.3 #34.7
pcaly_line1_amp = 93#
pcaly_line2_freq = 434.9
pcaly_line2_amp = 7200 #613.0 #Going to need to be tweaked up
pcaly_line3_freq = 1083.1
pcaly_line3_amp = 14500.0
pcaly_line4_freq = 283.31
pcaly_line4_amp = 200#1800

# Initlialisation values for pcal and sus subtraction
pcal_sub_init = {'line1':{'real':0.00376781,
                        'imag':0.00057292},
             'line2':{'real':5.29753e-06,
                        'imag':-1.90755e-07},
             'line3':{'real':8.45398e-07,
                        'imag':-7.27598e-08},
             'line4':{'real':1.24258e-05,
                        'imag':-2.81692e-07},
             'line5':{'real':-0.000888984,
                        'imag':-6.41558e-05},
             'line6':{'real':-0.000344493,
                        'imag':-9.88685e-06},
             'line7':{'real':-0.000165529,
                        'imag':-2.0269e-06},
             'line8':{'real':-9.51669e-05,
                        'imag':-1.23484e-05},
             'line9':{'real':-1.23484e-05,
                        'imag':1.07417e-07}}

sus_sub_init = {'line1':{'real':9.71212e-18,
                        'imag':1.86204e-18},
             'line2':{'real':2.12258e-15,
                        'imag':2.20994e-16},
             'line3':{'real':1.83012e-15,
                        'imag':3.8409e-16}}

# pcaly line phses changed to zero as per LLO:64539
pcaly_line1_phase = 0 #  -0.358154 
pcaly_line2_phase = 0 # -9.55591#Needs to be updated (absorbed into the kappa epics phase calculation) 
pcaly_line3_phase = 0 # -23.7986

#Do we use pcalx or pcaly for tdep?
use_pcaly = 1 #flip to 0 if pcalY dies
pcalx_good = 1 #switch to know if monitoring lines + HF lines should be on whilst use_pcaly=1  (i.e. flip to 0 if pcalX dies)

###################################################
# Single Arm Lock

arm_locked_threshold = 15

###################################################
# ADS settings

# Avoid 17.25Hz (BS bounce)

# quad dither settings
#itmx_p_freq = 8.3 #AE changed too much noise at 6.75 # MK 02/10/18 19.1  # Does not seem necessary, leave to SDF, CB AE, 20190130
#itmy_p_freq = 7.05 # MK 02/10/18 19.65 
#etmx_p_freq = 7.40 # MK 02/10/18 7.1 #6.7 #7.2	#7.4
#etmy_p_freq = 7.65 # MK 02/10/18 7.7 #7.4 #8.4	#7.95
#itmx_y_freq = 7.95 # MK 02/10/18 21.3  
#itmy_y_freq = 8.30 # MK 02/10/18 21.85
#etmx_y_freq = 8.61 # MK 02/10/18 8.1 #11.2	#8.3 
#etmy_y_freq = 8.90 # MK 02/10/18 8.8 #12.4	#8.65
quad_clk_gain = 300	#100 Increased AJM20190207	#Reduce to 30
quad_sin_gain = 1	#Increase to 3
quad_cos_gain = 1	#Increase to 3

# pr dither settings
pr2_ads_freq = {'PIT':17.85,
                'YAW':19.65}
pr2_p_freq = 17.85
pr2_y_freq = 19.65
pr3_p_freq = 13.15
pr3_y_freq = 14.85
pr_clk_gain = 0.4
pr_sin_gain = 1
pr_cos_gain = 1

#prm dither settings
prm_ads_freq = {'PIT':11.1,
                'YAW':12.7}
prm_p_freq = 11.1
prm_y_freq = 12.7
prm_clk_gain = 300
prm_sin_gain = 1
prm_cos_gain = 1

# sr dither settings
sr2_p_freq = 10.63		#16.15
sr2_y_freq = 11.97		#18.45
#sr3_p_freq = 12.25
#sr3_y_freq = 14.15
sr_clk_gain = 1
sr_sin_gain = 1
sr_cos_gain = 1

# demod phases
pit1_ph = 25
pit2_ph = -150
pit3_ph = 20   # MK 02/10/18  30 #-30 HY 08/28/2016
pit4_ph = -165 # MK 02/10/18 -30
yaw1_ph = 0
yaw2_ph = -180
yaw3_ph = -45  # MK 02/10/18 -30#-16 HY 08/28/2016
yaw4_ph = -25  # MK 02/10/18 -95#-30 HY 08/28/2016
pit5_ph = -40
yaw5_ph = -25

default_itmx_tcs_mask = 'ANNULAR'
default_itmy_tcs_mask = 'ANNULAR'

suspensions = ['SUS_RM1',
    'SUS_RM2',
    'SUS_IM1',
    'SUS_IM2',
    'SUS_IM3',
    'SUS_IM4',
    'SUS_MC1',
    'SUS_MC3',
    'SUS_PR3',
    'SUS_PRM',
    'SUS_MC2',
    'SUS_PR2',
    'SUS_BS',
    'SUS_ITMY',
    'SUS_ITMX',
    'SUS_SR2',
    'SUS_SRM',
    'SUS_SR3',
    'SUS_ETMY',
    'SUS_ETMX',
    'SUS_TMSX',
    'SUS_TMSY',
    'SUS_OM1',
    'SUS_OM2',
    'SUS_OM3',
    'SUS_OMC']

seismics = ['HPI_HAM1',
    'SEI_HAM2',
    'SEI_HAM3',
    'SEI_HAM4',
    'SEI_HAM5',
    'SEI_HAM6',
    'SEI_BS',
    'SEI_ITMX',
    'SEI_ITMY',
    'SEI_ETMX',
    'SEI_ETMY']



